from db.models import Utterance, Intent, Label
from sentence_transformers import SentenceTransformer
from src.backend_connection import BQDataSourceConnection

class UtteranceCollection:

    def __init__(self, config):
        self.embedder = SentenceTransformer(config.get_embedder_type())

    def add_utterance(self, utterance):
        '''
        Inserts a new utterance into the collection. If the utterance already exists, 
        its frequency is incremented by 1.
        '''
        existing_utterance = Utterance.objects.filter(
            message_text=utterance.message_text, context=utterance.context
        ).first()

        if existing_utterance:
            existing_utterance.frequency += 1
            existing_utterance.save()
        else:
            utterance.save()

    def delete_all(self):
        '''
        Deletes all utterances in the collection
        '''
        Utterance.objects.all().delete()

    def is_empty(self):
        '''
        Returns true if there are no utterances in the collection
        '''
        return Utterance.objects.count() == 0
    
    def insert_utterances(self, utterance_df, context='general'):
        '''
        Creates the first utterances in the collection from a data source
        '''
        for index, row in utterance_df.iterrows():
            utterance = Utterance(
                sample_message_id=row['original_id'],
                sample_user_id=row['chat_id'],
                message_text=row['message_text'],
                context=context,
                frequency=1
            )
            self.add_utterance(utterance)


    def get_utterance_batch(self, batch_size=10):
        '''
        Returns a batch of utterances from the collection
        '''
        return list(Utterance.objects.order_by('id')[:batch_size])