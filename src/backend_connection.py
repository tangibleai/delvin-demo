import pandas as pd
from google.cloud import bigquery
from google.oauth2 import service_account
import psycopg2
import json
import os, dotenv
import db_dtypes
import streamlit as st

import django
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
application = get_wsgi_application()
django.setup()

from db.models import Intent, Label, Utterance

dotenv.load_dotenv()


class BQDataSourceConnection:
    """
    Facilitates connection to BigQuery and retrieving data
    """
    def __init__(self, project_name, dataset_name):
        bq_key=os.environ.get('BQ_API_KEY')
        api_key_parsed = json.loads(bq_key)
        credentials = service_account.Credentials.from_service_account_info(
            api_key_parsed
        )
        self.client = bigquery.Client(credentials=credentials)
        self.project_name = project_name
        self.dataset_name = dataset_name
        self.messages_table = f"{project_name}.{dataset_name}.messages"



    def get_user_messages_dict(self, user_id):
        '''
        Returns all the messages with the given chat_id equal to user_id in dictionary format.
        '''
        query = f"""
                SELECT message_text, direction, inserted_at, bq_local_id
                FROM `{self.messages_table}`
                WHERE chat_id={user_id}
                ORDER BY bq_local_id
                """

        query_results = self.client.query(query).to_dataframe().to_dict(orient='records')
        return query_results

    def get_queries_df_by_response(self, response):
        query = f"""
                 SELECT
                    prev_id as original_id
                    ,prev_content as message_text
                    ,prev_inserted_at as inserted_at
                    ,prev_direction as direction
                    ,chat_id
                    FROM
                    (
                    SELECT
                        LAG(original_id,1) OVER (PARTITION BY chat_id ORDER BY bq_local_id) as prev_id
                        , LAG(message_text,1) OVER (PARTITION BY chat_id ORDER BY bq_local_id) prev_content
                        , LAG(inserted_at,1) OVER (PARTITION BY chat_id ORDER BY bq_local_id) prev_inserted_at
                        , LAG(direction,1) OVER (PARTITION BY chat_id ORDER BY bq_local_id) prev_direction
                        ,chat_id
                        ,direction
                        ,message_text
                            FROM  `{self.messages_table}`
                            WHERE 
                                chat_id IN
                                    (
                                        SELECT chat_id FROM `{self.messages_table}`
                                        WHERE message_text LIKE "%{response}%"
                                    ) 
                        ) 
                    where prev_direction='inbound' and message_text LIKE "%{response}%";
                    """
        print(query)
        query_results = self.client.query(query).to_dataframe()
        return query_results


class DjangoDB():
    """
    Factory class for all the custom objects
    """
    def create_intent(self, name, description=None, category=None, context=None):
        Intent.objects.create(name=name,
                              description=description,
                              category=category,
                              context = context)
        
    def get_intent_by_name(self, intent_name):
        return Intent.objects.filter(name=intent_name).first()
    
    def get_intents(self):
        '''
        Returns all the intents from the database
        '''
        return Intent.objects.all()
    

    def create_label(self, intent_id, utterance_id, confidence, labeler): 
        Label.objects.create(intent_id=intent_id,
                             utterance_id=utterance_id,
                             confidence=confidence,
                             labeler=labeler)


