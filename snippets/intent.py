import os,dotenv
import psycopg2
dotenv.load_dotenv()


class IntentTable():
    def __init__(self):
        self.conn = psycopg2.connect(
            host=os.environ.get("PG_HOST"),
            database=os.environ.get("PG_DB_NAME"),
            user=os.environ.get("PG_USER"),
            password=os.environ.get("PG_PASSWORD")
        )
        self.cursor = self.conn.cursor()

    def create_table(self):
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS your_table (
                id BIGINT DEFAULT extract(epoch from now())::bigint,
                name TEXT,
                description TEXT,
                category TEXT
            )
        """)
        self.conn.commit()

    def create(self, name, description, category):
        self.cursor.execute("""
            INSERT INTO your_table (name, description, category)
            VALUES (%s, %s, %s)
            RETURNING id
        """, (name, description, category))
        inserted_id = self.cursor.fetchone()[0]
        self.conn.commit()
        return inserted_id

    def get_by_id(self, id):
        self.cursor.execute("""
            SELECT * FROM your_table WHERE id = %s
        """, (id,))
        row = self.cursor.fetchone()
        return row

    def get_all(self):
        self.cursor.execute("""
            SELECT * FROM your_table
        """)
        rows = self.cursor.fetchall()
        return rows

    def update_by_id(self, id, name, description, category):
        self.cursor.execute("""
            UPDATE your_table
            SET name = %s, description = %s, category = %s
            WHERE id = %s
        """, (name, description, category, id))
        self.conn.commit()

    def delete_by_id(self, id):
        self.cursor.execute("""
            DELETE FROM your_table WHERE id = %s
        """, (id,))
        self.conn.commit()

    def __del__(self):
        self.cursor.close()
        self.conn.close()
