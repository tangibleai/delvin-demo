import psycopg2
import os, dotenv

dotenv.load_dotenv()
conn = psycopg2.connect(
            host=os.environ.get("PG_HOST"),
            database=os.environ.get("PG_DB_NAME"),
            user=os.environ.get("PG_USER"),
            password=os.environ.get("PG_PASSWORD")
        )
cursor = conn.cursor()

schema_name = os.environ.get("PG_SCHEMA")
table_name = 'intents'

cursor.execute(f"""
    SELECT EXISTS (
        SELECT 1
        FROM information_schema.tables
        WHERE table_schema = '{schema_name}'
        AND table_name = '{table_name}'
    )
""")
table_exists = cursor.fetchone()[0]

if not table_exists:
    # Create the table with the specified fields
    cursor.execute(f"""
        CREATE TABLE {schema_name}.{table_name} (
            id BIGINT DEFAULT extract(epoch from now())::bigint,
            name TEXT,
            description TEXT,
            category TEXT
        )
    """)
    conn.commit()
    print("Table created successfully.")
else:
    print("Table already exists.")



