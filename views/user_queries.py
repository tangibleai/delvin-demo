import streamlit as st
from stqdm import stqdm

import math

import hdbscan
from sentence_transformers import SentenceTransformer

from src.backend_connection import BQDataSourceConnection, DjangoDB
from src.config import AppConfig
from src.strings import USER_QUERIES_APP
from src.utterance_collection import UtteranceCollection

EXPLORE_TAB = "Explore"
LABEL_TAB = "Label"
INTENTS_TAB = "Intents"
MODEL_TAB = "Model"

@st.cache_resource
def load_embedder():
    embedder = SentenceTransformer('all-MiniLM-L6-v2')
    return embedder

@st.cache_data
def embed_queries(queries, _model):
    batch_size = 5
    num_batches = math.ceil(len(queries) / batch_size)
    encoded_pieces = []
    for i in stqdm(range(num_batches), desc="Encoding"):
        start_index = i * batch_size
        end_index = min((i + 1) * batch_size, len(queries))
        batch_pieces = queries[start_index:end_index]
        encoded_batch = _model.encode(batch_pieces)
        encoded_pieces.extend(encoded_batch)
    return encoded_pieces

def set_utterance_to_label(utterance):
    st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]['utterance_message_text'] = utterance.message_text
    st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]['utterance_id'] = utterance.id
    st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]['num_in_batch'] +=1


def annotate(label, db):
    intent_id = db.get_intent_by_name(label).id
    utterance_id = st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]['utterance_id']
    labeler = st.session_state['name']
    db.create_label(intent_id, utterance_id, 1.0, labeler)

    if st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]["utterances"]:
        set_utterance_to_label(st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]["utterances"][
                                   st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]['num_in_batch']
                               ])



def load_page(config: AppConfig,
              backend: BQDataSourceConnection,
              db: DjangoDB):
    
    intents = db.get_intents()
    uc = UtteranceCollection(config)
    intent_names = [intent.name for intent in intents]

    tabs = [EXPLORE_TAB,LABEL_TAB,INTENTS_TAB,MODEL_TAB]
    st.session_state["page_states"][USER_QUERIES_APP] = {key: {} for key in tabs}
    # st.write(st.session_state)

    explore_tab, label_tab, intent_tab, l_data_tab = st.tabs(tabs)

    queries = backend.get_queries_df_by_response("Sorry, I didn\'t understand what you said.")

    if uc.is_empty():
        uc.insert_utterances(queries)


    with explore_tab:
        st.title ("Explore")
        
        embedder=load_embedder()
        query_embeddings = embed_queries(list(queries["message_text"]), embedder)

        st.write('#')

            #cluster the queries with hdbscan
        clusterer = hdbscan.HDBSCAN(min_cluster_size=5, gen_min_span_tree=True)

        cluster_labels = clusterer.fit_predict(query_embeddings)
        unique_labels = set(cluster_labels)
        queries['cluster_num'] = cluster_labels
        is_clustered = st.checkbox("Clustered_view")

        st.write('#')

        if is_clustered:
            for i in unique_labels:
                cluster_queries = queries[queries['cluster_num'] == i]
                # put random query from cluster as cluster name
                cluster_name = cluster_queries['message_text'].sample().iloc[0]

                with st.expander(cluster_name):
                    for query in cluster_queries['message_text']:
                        st.write(query)
        else:
            st.dataframe(queries['message_text'], width=1000, height=800)
    
    with intent_tab:
        st.title ("Intents")

        st.header("Create New Intent")
        with st.form("New Intent", clear_on_submit=True):
            new_intent_name = st.text_input("Intent Name")
            new_intent_description = st.text_input("Intent Description")
            submitted = st.form_submit_button("Create")
            if submitted:
                db.create_intent(new_intent_name, new_intent_description)

        st.divider()
        intents = db.get_intents()
        for i in range(len(intents)):
            intent = intents[i]
            col_details, col_buttons = st.columns([4, 1])
            
            with col_details:
                st.write(f"## {intent.name}")
                st.write(f"**Description:** {intent.description}")
            with col_buttons:
                st.write('##')
                key_edit = f"edit_intent_button_{i}"
                key_delete = f"delete_intent_button_{i}"
                st.button("Edit", key=f"edit_intent_button_{i}")
                st.button("Delete", key=f"delete_intent_button_{i}")

            st.divider()

    with label_tab:
        state = st.session_state['page_states'][USER_QUERIES_APP][LABEL_TAB]
        state["num_in_batch"] = 0
        st.title ("Label User Queries")

        st.header("Singly-Query Labeling")
        st.markdown("Manually label intents with low confidence")

        state["utterances"] = uc.get_utterance_batch()
        set_utterance_to_label(state["utterances"][state["num_in_batch"]])
        st.write(state["utterances"])

        col_query, col_intent = st.columns([3, 1])
        with col_query:
            st.text_area("Utterance",
                        placeholder=state['utterance_message_text'],
                        disabled=True) 

        with col_intent:
            intent_label_1 = st.selectbox("Intent Label 1", intent_names)
        # intent_label_2 = st.multiselect("Intent Label 2 (optional)", intent_names)
        # intent_label_3 = st.multiselect("Intent Label 3 (optional)", intent_names)

        col_spacer, col_skip, col_label, col_spacer2 = st.columns([4, 1,1,4])
        with col_skip:
            st.button("Skip")

        with col_label:
            st.button("Label", on_click=annotate, args=(intent_label_1,db))


        st.header('Intent Enrichment')

        sample_utterance = st.text_input("Similar to")
        candidates = ['how much does the pill cost', 'I want to know about pills', 'where do I find pills']

        if sample_utterance:
            col_candidates, col_dropdown = st.cols([3,1])

            enrich_checkboxes = []*len(candidates)
            with col_candidates:
                for candidate in candidates:
                    enrich_cb_res = st.checkbox(candidate)


            with col_dropdown:
                enrich_label = st.selectbox(intent_names)



