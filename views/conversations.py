import streamlit as st
from streamlit_chat import message
from src.backend_connection import BQDataSourceConnection


def load_page(config, backend):
    st.title("Conversation Explorer")

    chat_id = st.text_input("Search by chat_id")

    if chat_id:
        messages = backend.get_user_messages_dict(chat_id)

        for m in messages:
            if m['direction'] == 'inbound':
                message(m["message_text"],
                        avatar_style='open-peeps',
                        is_user=True,
                        key=m["bq_local_id"])
            else:
                message(m["message_text"],
                        avatar_style='bottts-neutral',
                        key = m["bq_local_id"])