import streamlit as st
from src.config import AppConfig
from PIL import Image
import requests


def load_page(config: AppConfig):
    col_title, col_logo = st.columns([4,1])
    with col_title:
        st.title(config.get_project_title()+ ' - Analytics')

    with col_logo:
        st.image(config.get_project_logo())

    dashboards = config.get_analytics_dashboards()
    dashboard_names = [d['name'] for d in dashboards]
    tabs = st.tabs(dashboard_names)

    for i in range(len(tabs)):
        with tabs[i]:
            st.components.v1.iframe(
                dashboards[i]["url"],
                height=1200,
                scrolling=True)