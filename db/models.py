from django.db import models


class Intent(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    description = models.TextField(blank=True, null=True)
    category = models.CharField(max_length=100, blank=True, null=True)
    context = models.CharField(max_length=100, blank=True, null=True)



class Utterance(models.Model):
    id = models.AutoField(primary_key=True)
    sample_message_id = models.CharField(max_length=100, blank=True, null=True)
    message_text = models.TextField()
    frequency = models.IntegerField(default=1)
    sample_user_id = models.CharField(max_length=100, blank=True, null = True)
    context = models.CharField(max_length=100, blank=True, null=True)

class Label(models.Model):
    id = models.AutoField(primary_key=True)
    intent = models.ForeignKey(Intent, on_delete=models.CASCADE)
    utterance = models.ForeignKey(Utterance, on_delete=models.CASCADE)
    confidence = models.FloatField()
    labeler = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)