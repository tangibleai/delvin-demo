import os


import streamlit as st
from streamlit_option_menu import option_menu

from views import analytics, conversations, user_queries
from src.auth import create_authenticator
from src.backend_connection import BQDataSourceConnection, DjangoDB
from src.config import AppConfig
from src.styles import set_page_container_style
from src.ui import menu_icons



import math

st.set_page_config(layout='wide')
set_page_container_style()


def create_page_structure():

    config = AppConfig()
    apps = config.get_apps()
    st.session_state['page_states'] = dict.fromkeys(apps, None)
    (project_name, dataset_name) = config.get_bq_details()

    data_source = BQDataSourceConnection(project_name=project_name,
                                     dataset_name=dataset_name)

    db = DjangoDB()

    icons = [menu_icons[app] for app in apps]
    styles = {
        "icon": {"color": "#4141f7", "font-size": "18px"},
        "nav-link": {"font-size": "18px", "text-align": "left", "margin": "0px", "--hover-color": "#A29BFE"},
        "nav-link-selected": {"background-color": "#4141f7"},
    }

    with st.sidebar:
        st.image("./assets/delvin_logo_270.png")

        selected = option_menu(
            menu_title=None,  # required
            options=apps,  # required
            icons=icons,  # optional
            menu_icon="menu-down",  # optional
            default_index=0,
            styles=styles# optional
        )

        space_length = 10 - math.ceil(1.5*len(icons))
        for i in range(space_length):
            st.write('#')

        st.image("./assets/powered_by_tai.png", width=270)

    if selected == "Analytics":
        analytics.load_page(config)

    elif selected == "Conversations":
        conversations.load_page(config,data_source)

    elif selected == "User Queries":
        user_queries.load_page(config,data_source,db)


authenticator = create_authenticator()
name, authentication_status, username = authenticator.login('Login', 'main')

if authentication_status:
    create_page_structure()